//
//  ImagesCell.swift
//  DemoApp
//
//  Created by Ma Kim Tai on 9/3/19.
//  Copyright © 2019 ms.apptech.com. All rights reserved.
//

import UIKit
import SDWebImage
class ImagesCell: UICollectionViewCell {

    @IBOutlet weak var lbl: UILabel!
    @IBOutlet weak var imv: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func updateCell(_ item:ContentDTO)  {
        lbl.text = item.name ?? ""
        imv.sd_setImage(with: item.imageUrl, completed: nil)
    }

}
