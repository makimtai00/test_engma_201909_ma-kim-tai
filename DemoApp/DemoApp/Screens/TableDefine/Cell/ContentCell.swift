//
//  ContentCell.swift
//  DemoApp
//
//  Created by Ma Kim Tai on 9/3/19.
//  Copyright © 2019 ms.apptech.com. All rights reserved.
//

import UIKit
import SDWebImage

class ContentCell: UITableViewCell {

    @IBOutlet weak var imvBg: UIImageView!
    @IBOutlet weak var vImgCircle: UIView!
    @IBOutlet weak var imvAva: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDes: UILabel!
    @IBOutlet weak var clvThumbImage: UICollectionView!
    private var dataSource:ClvSource?
    private var items:[ContentDTO] = []
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        dataSource = ClvSource(clvThumbImage, items: [])
        dataSource?.isThumbImage = true
        vImgCircle.layer.cornerRadius = 5
        imvAva.layer.cornerRadius = 5
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateCell(_ item:ContentDTO) {
        lblTitle.text = item.name ?? ""
        lblDes.text = "Thissssss isssss short descriptionnnnnnnnnnnnnnnnnnnnnnnnnn"
        imvBg.sd_setImage(with: item.imageUrl, completed: nil)
        imvAva.sd_setImage(with: item.imageUrl, completed: nil)
        dataSource?.items = [ContentDTO(name: "", imageUrl: item.imageUrl),ContentDTO(name: "", imageUrl: item.imageUrl),ContentDTO(name: "", imageUrl: item.imageUrl)]
        clvThumbImage.reloadData()
        
    }
    
}
