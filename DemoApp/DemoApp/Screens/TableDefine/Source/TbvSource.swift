//
//  TbvSource.swift
//  DemoApp
//
//  Created by Ma Kim Tai on 9/3/19.
//  Copyright © 2019 ms.apptech.com. All rights reserved.
//

import UIKit

class TbvSource: NSObject,UITableViewDataSource , UITableViewDelegate {
    var items:[ContentDTO] = []
    var handleLoadMore:(()->())?
    private var tbv:UITableView!
    var isLoading:Bool = false
    init(_ tbv:UITableView,items:[ContentDTO]) {
        super.init()
        self.items = items
        self.tbv = tbv
        tbv.dataSource = self
        tbv.delegate = self
        
        tbv.register(UINib(nibName: String(describing: ContentCell.self), bundle: nil), forCellReuseIdentifier: String(describing: ContentCell.self))
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ContentCell.self)) as? ContentCell
        cell?.updateCell(items[indexPath.row])
        return cell ?? UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastSectionIndex = tableView.numberOfSections - 1
        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
        if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex {
            if !isLoading{
                isLoading = true
                let spinner = UIActivityIndicatorView(style: .gray)
                spinner.startAnimating()
                spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
                
                tableView.tableFooterView = spinner
                tableView.tableFooterView?.isHidden = false
                handleLoadMore?()
            }
        }
    }
}
