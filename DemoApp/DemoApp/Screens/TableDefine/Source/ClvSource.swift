//
//  ClvSource.swift
//  DemoApp
//
//  Created by Ma Kim Tai on 9/3/19.
//  Copyright © 2019 ms.apptech.com. All rights reserved.
//

import UIKit

class ClvSource: NSObject,UICollectionViewDataSource, UICollectionViewDelegate {
    
    var items:[ContentDTO] = []
    var isThumbImage:Bool = false
    init(_ clv:UICollectionView, items:[ContentDTO]) {
        super.init()
        self.items = items
        clv.dataSource = self
        clv.delegate = self
        clv.register(UINib(nibName: String(describing: ImagesCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: ImagesCell.self))
        
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: ImagesCell.self), for: indexPath) as? ImagesCell
        cell?.updateCell(items[indexPath.row])
        return cell ?? UICollectionViewCell()
    }
    
}

extension ClvSource:UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        guard let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout else { return CGSize(width: 100, height: 100) }
        if isThumbImage {
            layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            layout.minimumLineSpacing = 4
            return CGSize(width: collectionView.bounds.width / 3.1, height: collectionView.bounds.height)
        }
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        return CGSize(width: collectionView.bounds.width / 4, height: collectionView.bounds.height)
    }
}
