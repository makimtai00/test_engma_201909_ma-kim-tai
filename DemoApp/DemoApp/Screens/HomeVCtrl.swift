//
//  HomeVCtrl.swift
//  DemoApp
//
//  Created by Ma Kim Tai on 9/3/19.
//  Copyright © 2019 ms.apptech.com. All rights reserved.
//

import UIKit
import FlickrKit

class HomeVCtrl: UIViewController {
    
    @IBOutlet weak var tbv: UITableView!
    @IBOutlet weak var clv: UICollectionView!
    
    var items:[ContentDTO] = []
    var itemScrollX:Int = 0
    private var pageIndex:Int = 1
    private var pageCount:Int = 0
    private var dataTbvSource:TbvSource?
    private var dataClvSource:ClvSource?
    override func viewDidLoad() {
        super.viewDidLoad()
        initSource()
        getData()
        let _ = Timer.scheduledTimer(withTimeInterval: 2, repeats: true, block: { _ in
            self.autoScrollClv()
        })
    }
    
    @objc private func autoScrollClv() {
        if dataClvSource?.items.isEmpty ?? false {
            return
        }
        UIView.animate(withDuration: 1, animations: {
            if self.itemScrollX < (self.dataClvSource?.items.count ?? 0) {
                let indexPath = IndexPath(item: self.itemScrollX, section: 0)
                self.clv.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: false)
                self.itemScrollX += 2
            } else {
                self.itemScrollX = 0
                self.clv.scrollToItem(at: IndexPath(item: 0, section: 0), at: .centeredHorizontally, animated: true)
            }
        })
    }
    
    private func initSource() {
        self.dataTbvSource = TbvSource(tbv,items: [])
        self.dataClvSource = ClvSource(clv,items: [])
        
        self.dataTbvSource?.handleLoadMore = {
            self.getData(true)
        }
        
    }
    
    private func getData(_ isLoadMore:Bool = false) {
        if pageIndex >= pageCount && isLoadMore {
            self.tbv.tableFooterView?.isHidden = true
            self.tbv.tableFooterView = nil
            self.dataTbvSource?.isLoading = false
            return
        }
        let request = FKFlickrInterestingnessGetList()
        request.per_page = "20"
        request.page = "\(self.pageIndex)"
        FlickrKit.shared().call(request) { (respnose , error ) in
            DispatchQueue.main.async {
                if let res = respnose {
                    let topPhotos = res["photos"] as! [String: AnyObject]
                    let photoArray = topPhotos["photo"] as! [[NSObject: AnyObject]]
                    for photoDictionary in photoArray {
                        let item = ContentDTO(name: "", imageUrl: nil)
                        let photoURL = FlickrKit.shared().photoURL(for: FKPhotoSize.small240, fromPhotoDictionary: photoDictionary)
                        item.imageUrl = photoURL
                        item.name = photoDictionary["title" as NSObject] as? String ?? ""
                        self.items.append(item)
                        
                    }
                    self.pageIndex += 1
                    self.pageCount = topPhotos["pages"] as? Int ?? 0
                    self.dataTbvSource?.isLoading = false
                    if !isLoadMore {
                        self.dataClvSource?.items = self.items
                        self.dataTbvSource?.items = self.items
                        self.tbv.reloadData()
                        self.clv.reloadData()
                    } else {
                        self.dataTbvSource?.items += self.items
                        self.tbv.reloadData()
                    }
                    self.tbv.tableFooterView?.isHidden = true
                    self.tbv.tableFooterView = nil
                }
            }
        }
    }
}
