//
//  ContentDTO.swift
//  DemoApp
//
//  Created by Lam Hong Bac on 9/3/19.
//  Copyright © 2019 ms.apptech.com. All rights reserved.
//

import UIKit

class ContentDTO: NSObject {
    var name:String?
    var imageUrl:URL?
    
    init(name:String, imageUrl:URL?) {
        super.init()
        self.name = name
        self.imageUrl = imageUrl
    }
}
